\begin{thebibliography}{10}

\bibitem{AinsworthO-00}
M.~Ainsworth and J.~Oden.
\newblock {\em A posteriori error estimation in finite element analysis}.
\newblock Wiley Interscience, 2000.

\bibitem{HughesErrorIGA}
Y.~Bazilevs, L.~Beirao Da~Veiga, J.~A. Cottrell, T.~J.~R. Hughes, and
  G.~Sangalli.
\newblock Isogeometric analysis: approximation, stability and error estimates
  for \emph{h}-refined meshes.
\newblock {\em Mathematical Models and Methods in Applied Sciences},
  16(07):1031--1090, 2006.

\bibitem{Bazilevs2008}
Y.~Bazilevs, V.~M. Calo, T.~J.~R. Hughes, and Y.~Zhang.
\newblock Isogeometric fluid-structure interaction: theory, algorithms, and
  computations.
\newblock {\em Computational Mechanics}, 43(1):3--37, 2008.

\bibitem{beer2015advancedbook}
G.~Beer.
\newblock {\em Advanced Numerical Simulation Methods: From CAD Data Directly to
  Simulation Results}.
\newblock CRC Press, 2015.

\bibitem{beer2016advanced}
G.~Beer and C.~Duenser.
\newblock Advanced boundary element analysis of geotechnical problems with
  geological inclusions.
\newblock {\em Computers and Geotechnics}, 79:86--95, 2016.

\bibitem{Beer-17-GIFT_Flow}
G.~Beer, V.~Mallardo, E.~Ruocco, and C.~D{\"u}nser.
\newblock Isogeometric boundary element analysis of steady incompressible
  viscous flow, {Part 1}: Plane problems.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  326C:51--69, 2017.

\bibitem{beer2017isogeometric}
G.~Beer, V.~Mallardo, E.~Ruocco, B.~Marussig, J.~Zechner, C.~D{\"u}nser, and
  T.-P. Fries.
\newblock Isogeometric boundary element analysis with elasto-plastic
  inclusions. {Part 2: 3-D} problems.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  315:418--433, 2017.

\bibitem{beer2015simple}
G.~Beer, B.~Marussig, and J.~Zechner.
\newblock A simple approach to the numerical simulation with trimmed cad
  surfaces.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  285:776--790, 2015.

\bibitem{beer2016isogeometric}
G.~Beer, B.~Marussig, J.~Zechner, C.~D{\"u}nser, and T.-P. Fries.
\newblock Isogeometric boundary element analysis with elasto-plastic
  inclusions. {Part 1}: Plane problems.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  308:552--570, 2016.

\bibitem{beirao2013analysis}
L.~Beir{\~a}o~da Veiga, A.~Buffa, G.~Sangalli, and R.~V{\'a}zquez.
\newblock Analysis-suitable t-splines of arbitrary degree: definition, linear
  independence and approximation properties.
\newblock {\em Mathematical Models and Methods in Applied Sciences},
  23(11):1979--2003, 2013.

\bibitem{Benson2010276}
D.~Benson, Y.~Bazilevs, M.~Hsu, and T.~Hughes.
\newblock Isogeometric shell analysis: The reissner--mindlin shell.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  199(5--8):276 -- 289, 2010.

\bibitem{Bressan201517}
A.~Bressan, A.~Buffa, and G.~Sangalli.
\newblock Characterization of analysis-suitable t-splines.
\newblock {\em Computer Aided Geometric Design}, 39:17 -- 49, 2015.

\bibitem{buffa2010isogeometric}
A.~Buffa, G.~Sangalli, and R.~V{\'a}zquez.
\newblock Isogeometric analysis in electromagnetics: B-splines approximation.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  199(17):1143--1152, 2010.

\bibitem{coox2016robust}
L.~Coox, F.~Greco, O.~Atak, D.~Vandepitte, and W.~Desmet.
\newblock A robust patch coupling method for nurbs-based isogeometric analysis
  of non-conforming multipatch surfaces.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  316(1):235--260, 2017.

\bibitem{da2011isogeometric}
L.~B. da~Veiga, A.~Buffa, D.~Cho, and G.~Sangalli.
\newblock Isogeometric analysis using t-splines on two-patch geometries.
\newblock {\em Computer methods in applied mechanics and engineering},
  200(21):1787--1803, 2011.

\bibitem{de2011x}
E.~De~Luycker, D.~Benson, T.~Belytschko, Y.~Bazilevs, and M.~Hsu.
\newblock X-fem in isogeometric analysis for linear fracture mechanics.
\newblock {\em International Journal for Numerical Methods in Engineering},
  87(6):541--565, 2011.

\bibitem{Deng200876}
J.~Deng, F.~Chen, X.~Li, C.~Hu, W.~Tong, Z.~Yang, and Y.~Feng.
\newblock Polynomial splines over hierarchical t-meshes.
\newblock {\em Graphical Models}, 70(4):76 -- 86, 2008.

\bibitem{du2015nitsche}
X.~Du, G.~Zhao, and W.~Wang.
\newblock Nitsche method for isogeometric analysis of reissner--mindlin plate
  with non-conforming multi-patches.
\newblock {\em Computer Aided Geometric Design}, 35:121--136, 2015.

\bibitem{HierarchicalBSplines1}
D.~R. Forsey and R.~H. Bartels.
\newblock {Hierarchical B-spline Refinement}.
\newblock {\em SIGGRAPH Comput. Graph.}, 22(4):205--212, June 1988.

\bibitem{THB1}
C.~Giannelli, B.~J{\"u}ttler, S.~Kleiss, A.~Mantzaflaris, B.~Simeon, and
  J.~Špeh.
\newblock {THB-splines: An effective mathematical technology for adaptive
  refinement in geometric design and isogeometric analysis}.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  299:337--365, 2016.

\bibitem{THB0}
C.~Giannelli, B.~J{\"u}ttler, and H.~Speleers.
\newblock {THB-splines: The truncated basis for hierarchical splines}.
\newblock {\em Computer Aided Geometric Design}, 29(7):485 -- 498, 2012.

\bibitem{Hughes20054135}
T.~Hughes, J.~Cottrell, and Y.~Bazilevs.
\newblock Isogeometric analysis: Cad, finite elements, nurbs, exact geometry
  and mesh refinement.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  194(39--41):4135 -- 4195, 2005.

\bibitem{khajah2016isogeometric}
T.~Khajah, X.~Antoine, and S.~Bordas.
\newblock Isogeometric finite element analysis of time-harmonic exterior
  acoustic scattering problems.
\newblock {\em arXiv preprint arXiv:1610.01694}, 2016.

\bibitem{Li2016}
X.~Li, F.~Chen, H.~Kang, and J.~Deng.
\newblock A survey on the local refinable splines.
\newblock {\em Science China Mathematics}, 59(4):617--644, 2016.

\bibitem{lian2012recent}
H.~Lian, S.~Bordas, R.~Sevilla, and R.~Simpson.
\newblock Recent developments in cad/analysis integration.
\newblock {\em arXiv preprint arXiv:1210.8216}, 2012.

\bibitem{lian2017shape}
H.~Lian, P.~Kerfriden, and S.~Bordas.
\newblock Shape optimization directly from cad: An isogeometric boundary
  element approach using t-splines.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  317:1--41, 2017.

\bibitem{lian2015implementation}
H.~Lian, P.~Kerfriden, and S.~P.~A. Bordas.
\newblock Implementation of regularized isogeometric boundary element methods
  for gradient-based shape optimization in two-dimensional linear elasticity.
\newblock {\em International Journal for Numerical Methods in Engineering},
  106(12):972--1017, 2016.

\bibitem{lian2013stress}
H.~Lian, R.~N. Simpson, and S.~Bordas.
\newblock Stress analysis without meshing: Isogeometric boundary-element
  method.
\newblock {\em Proceedings of the Institution of Civil Engineers: Engineering
  and Computational Mechanics}, 166(2):88--99, 2013.

\bibitem{marussig2015fast}
B.~Marussig, J.~Zechner, G.~Beer, and T.-P. Fries.
\newblock Fast isogeometric boundary element method based on independent field
  approximation.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  284:458--488, 2015.

\bibitem{NguyenP-16}
T.~Nguyen and J.~Peters.
\newblock Refinable {$C^{1}$} spline elements for irregular quad layout.
\newblock {\em Computer Aided Geometric Design}, 43:123--130, 2016.

\bibitem{Nguyen201589}
V.~P. Nguyen, C.~Anitescu, S.~P. Bordas, and T.~Rabczuk.
\newblock Isogeometric analysis: An overview and computer implementation
  aspects.
\newblock {\em Mathematics and Computers in Simulation}, 117:89 -- 116, 2015.

\bibitem{nguyen2014nitsche}
V.~P. Nguyen, P.~Kerfriden, M.~Brino, S.~P. Bordas, and E.~Bonisoli.
\newblock Nitsche's method for two and three dimensional nurbs patch coupling.
\newblock {\em Computational Mechanics}, 53(6):1163--1182, 2014.

\bibitem{NguyenThanh20111892}
N.~Nguyen-Thanh, H.~Nguyen-Xuan, S.~Bordas, and T.~Rabczuk.
\newblock Isogeometric analysis using polynomial splines over hierarchical
  t-meshes for two-dimensional elastic solids.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  200(21--22):1892 -- 1908, 2011.

\bibitem{xuan2016isogeometric}
X.~Peng.
\newblock Isogeometric boundary element methods for linear elastic fracture
  mechanics.
\newblock Technical report, University of Luxembourg, Luxembourg, 2016.
\newblock http://orbilu.uni.lu/handle/10993/25835.

\bibitem{peng2016linear}
X.~Peng, E.~Atroshchenko, P.~Kerfriden, and S.~Bordas.
\newblock Linear elastic fracture simulation directly from cad: 2d nurbs-based
  implementation and role of tip enrichment.
\newblock {\em International Journal of Fracture}, 204(1):55--78, 2016.

\bibitem{peng2017isogeometric}
X.~Peng, E.~Atroshchenko, P.~Kerfriden, and S.~Bordas.
\newblock Isogeometric boundary element methods for three dimensional static
  fracture and fatigue crack growth.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  316:151--185, 2017.

\bibitem{NURBSbook}
L.~Piegl and W.~Tiller.
\newblock {\em The NURBS book}.
\newblock Springer, 1997.

\bibitem{Reali}
A.~Reali.
\newblock An iso geometric analysis approach for the study of structural
  vibrations.
\newblock {\em Journal of Earthquake Engineering}, 10(sup001):1--30, 2006.

\bibitem{Reif-97}
U.~Reif.
\newblock A refineable space of smooth spline surfaces of arbitrary topological
  genus.
\newblock {\em Journal of Approximation Theory}, 90:174--199, 1997.

\bibitem{SchillingerDSEBRH-12}
D.~Schillinger, L.~Dede, M.~Scott, J.~Evans, M.~Borden, E.~Rank, and T.~Hughes.
\newblock {An isogeometric design-through-analysis methodology based on
  adaptive hierarchical refinement of {NURBS}, immersed boundary methods, and
  {T}-spline {CAD} surfaces}.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  249-252:116--150, 2012.

\bibitem{scott2012local}
M.~Scott, X.~Li, T.~Sederberg, and T.~Hughes.
\newblock Local refinement of analysis-suitable t-splines.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  213:206--222, 2012.

\bibitem{Scott2013197}
M.~Scott, R.~Simpson, J.~Evans, S.~Lipton, S.~Bordas, T.~Hughes, and
  T.~Sederberg.
\newblock Isogeometric boundary element analysis using unstructured t-splines.
\newblock {\em Computer Methods in Applied Mechanics and Engineering}, 254:197
  -- 221, 2013.

\bibitem{SederbergCFNZL-04-TSplines}
T.~Sederberg, D.~Cardon, G.~Finnigan, N.~North, J.~Zheng, and T.~Lyche.
\newblock T-spline simplification and local refinement.
\newblock {\em ACM Trans. Graph.}, 23(3):276--283, Aug. 2004.

\bibitem{SederbergZBN-03-TSplines}
T.~Sederberg, J.~Zheng, A.~Bakenov, and A.~Nasri.
\newblock T-splines and {T-NURCCs}.
\newblock {\em ACM Trans. Graph.}, 22(3):477--484, Aug. 2003.

\bibitem{seo2010shape}
Y.-D. Seo, H.-J. Kim, and S.-K. Youn.
\newblock Shape optimization and its extension to topological design based on
  isogeometric analysis.
\newblock {\em International Journal of Solids and Structures},
  47(11):1618--1640, 2010.

\bibitem{Simpson201287}
R.~Simpson, S.~Bordas, J.~Trevelyan, and T.~Rabczuk.
\newblock A two-dimensional isogeometric boundary element method for
  elastostatic analysis.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  209--212:87 -- 100, 2012.

\bibitem{Simpson2014265}
R.~Simpson, M.~Scott, M.~Taus, D.~Thomas, and H.~Lian.
\newblock Acoustic isogeometric boundary element analysis.
\newblock {\em Computer Methods in Applied Mechanics and Engineering}, 269:265
  -- 290, 2014.

\bibitem{Stummel-80}
F.~Stummel.
\newblock The limitations of the patch test.
\newblock {\em Inernat. J. Numer. Methods Engrg.}, 15:177--188, 1980.

\bibitem{ToshniwalSH-17}
D.~Toshniwal, H.~Speleers, and T.~Hughes.
\newblock Smooth cubic spline spaces on unstructured quadrilateral meshes with
  particular emphasis on extraordinary points: {G}eometric design and
  isogeometric analysis considerations.
\newblock {\em Computer Methods in Applied Mechanics and Engineering}, 2017.
\newblock http://dx.doi.org/10.1016/j.cma.2017.06.008.

\bibitem{HierarchicalBSplines2}
A.-V. Vuong, C.~Giannelli, B.~J{\"u}ttler, and B.~Simeon.
\newblock A hierarchical approach to adaptive local refinement in isogeometric
  analysis.
\newblock {\em Computer Methods in Applied Mechanics and Engineering},
  200(49–52):3554 -- 3567, 2011.

\bibitem{Wang20111438}
P.~Wang, J.~Xu, J.~Deng, and F.~Chen.
\newblock Adaptive isogeometric analysis using rational pht-splines.
\newblock {\em Computer-Aided Design}, 43(11):1438 -- 1448, 2011.

\bibitem{xu13cada}
G.~Xu, B.~Mourrain, R.~Duvigneau, and A.~Galligo.
\newblock Analysis-suitable volume parameterization of multi-block
  computational domain in isogeometric applications.
\newblock {\em Computer-Aided Design}, 45(2):395 -- 404, 2013.

\bibitem{ZhangLi-16-TSplines}
J.~Zhang and X.~Li.
\newblock On degree elevation of {T}-splines.
\newblock {\em Computer Aided Geometric Design}, 46:16 -- 29, 2016.

\bibitem{ZienkiewiczTZ-FEM_V1}
O.~Zienkiewicz, R.~Taylor, and J.~Zhu.
\newblock {\em The finite element method: Its basis and fundamentals},
  volume~1.
\newblock Elsevier, 2013.

\end{thebibliography}
