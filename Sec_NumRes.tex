\section{Numerical results}\label{num_examples}
In this section, we present convergence results of various bases choices for the geometry and the numerical solution. We consider two classes of problems, namely, Laplace/Poisson problem and the problem of linear elasticity.
%

%
In Section~\ref{sec:desc_examples}, we first present problem data of all the numerical examples, i.e. analytic solution, boundary data, forcing function, geometry parametrization, and the basis for numerical solution. In Section~\ref{sec:conv_examples}, we discuss the convergence results of these examples. In Section~\ref{sec:res_disc}, we comment on the relation of the convergence results with patch-tests, and relate with the asymptotic theoretical convergence. Finally, in Section~\ref{sec:naming}, we propose new naming convention for the choices of bases orders with respect to the isogeometric paradigm.
%
\subsection{Description of various examples}\label{sec:desc_examples}
%
\begin{exmp}\label{exmp:lap_annu}
Consider the Laplace problem in the quarter annulus domain of Fig.~\ref{fig:qannu}, together with the exact solution
\begin{equation}
u (r,\theta)= r^{-3}\cos(3\theta),
\end{equation}
which is prescribed as Dirichlet boundary condition on all boundaries. This geometry is exactly represented by the NURBS basis $\mathcal{N}_{1, 2}$. The convergence study cases are based on the choices of geometry and solution bases which are used in the patch tests in Section~\ref{patch_tests} (see Table~\ref{tab:res_tests}).
%
\end{exmp}
%


%
\begin{exmp}\label{exmp:elas_cyl}
Consider the problem of a thick walled cylinder under a uniform internal and external pressure, as shown in Fig.~\ref{cylinder}. Instead of the full problem, a quarter annulus is considered with the symmetry boundary conditions along $x = 0$ and $y = 0$, and the following boundary conditions on the inner and outer boundary of the cylinder are imposed:
%


%
\begin{minipage}{.49\textwidth}
\centering
\includegraphics[width=0.65\textwidth]{Figures/cylinders}
\captionof{figure}{Thick-walled pressurized cylinder, \\Example~\ref{exmp:elas_cyl}.}
\label{cylinder}
\end{minipage}%
\begin{minipage}{.49\textwidth}
\centering
\includegraphics[width=0.99\textwidth]{Figures/plot_wedge}
\vspace*{-5mm}\captionof{figure}{Wedge geometry, \\Example~\ref{exmp:lap_wedge}.\vspace*{5mm}}
\label{wedge_geo}
\end{minipage}
%

%
\begin{equation}
\sigma_{r}(r_{1}) = -p_{1}, \qquad \sigma_{r}(r_{2}) = -p_{2}
\end{equation}
%
The solution of this problem in polar coordinates, under plane strain assumption, is given by
%
\begin{align}
\sigma_{r} (r)= s_{1}r^{-2} + s_{2}, \quad
\sigma_{\theta} (r)= -s_{1}r^{-2} + s_{2}, \quad
u_{r} (r)=  s_{3} \left(- s_{1} r^{-1} + s_{2} r\right),
\end{align}
%
where $s_{1} = \dfrac{r_{1}^2 r_{2}^2 (p_{2} - p_{1})}{r_{2}^2 - r_{1}^2}$, $s_{2} = (1 - 2 \nu) \dfrac{r_{1}^2 p_{1} - r_{2}^2 p_{2}}{r_{2}^2 - r_{1}^2}$, and $s_{3} = \dfrac{1 + \nu}{E}$.
%
The numerical tests were performed on the same choices of the combination of geometry-solution bases, as in Example~\ref{exmp:lap_annu} for Laplace equation (see also Table~\ref{tab:res_tests}).
\end{exmp}
%


%
\begin{exmp}\label{exmp:lap_wedge}
Consider the Laplace problem in a wedge geometry, see Fig.~\ref{wedge_geo}, together with the exact solution
%
\begin{equation}
u = \log((x + 0.1)^2 + (y + 0.1)^2),
\end{equation}
%
which is prescribed as Dirichlet boundary condition on all boundaries.
%
The choice of this geometry is motivated by the observation in the super-parametric case of Example~\ref{exmp:lap_annu}.
%
As opposed to the patch test cases, where the degree for geometry representation was artificially lifted, the geometry in this case is given by a tensor product of NURBS of degrees $p_{\xi} = 1$ and $p_{\eta} = 4$.
%
In this example, we focus on sub- and super- parametric solution approximations. As the geometry parameterization is fixed (we denote the corresponding NURBS basis by $\mathcal{N}_{1,4}$), to ease with the naming, we use the notation of paired bases as $(\mathcal{N}_{1,4}, S_{k,l})$, where $S_{k,l}$ denotes the basis for solution approximation ($S = \mathcal{N}$ for NURBS, as well as $S = \mathcal{B}$ for B-splines), $k$ denotes the degree in $\xi$ direction, and $l$ denotes the degree in $\eta$ direction. For sub-parametric solution approximations, we only elevate the degree in $\xi$ direction, i.e. $k = 2$, and for super-parametric solution approximations, we only consider degree reduction in $\eta$ direction, i.e. $1 \le l \le 3$.
\end{exmp}
%


%
\begin{exmp}\label{exmp:elas_plate}
\begin{figure}[!ht]
\centering
\begin{subfigure}[b]{0.425\textwidth}
\includegraphics[width=\textwidth]{Figures/example_plate.pdf}
\caption{Quarter of the plate with a hole, subjected to the boundary conditions given by Eq.~\eqref{plate_solution}.}
\label{plate}
\end{subfigure}
\quad
\begin{subfigure}[b]{0.425\textwidth}
\includegraphics[width=\textwidth]{Figures/plate_param.pdf}
\caption{Control net and element boundaries in the plate parametrization.}
\label{plate_param}
\end{subfigure}
\caption{Geometry and parameterization, Example~\ref{exmp:elas_plate}.}
\end{figure}
%
Consider a typical problem of a plate weakened by a circular hole of radius $a$, and subject to remote tension $T$ in $x$-direction. Only a quarter of a plate (of a finite size $L = 4 a$), as shown in Fig.~\ref{plate}, is modeled with the symmetry boundary conditions along $x = 0$ and $y = 0$, and analytical tractions are prescribed on the rest of the boundary according to the solution below:
%
\begin{equation}
\begin{split}
\sigma_{xx} (r,\theta)&= T - T\dfrac{a^2}{r^2}\left(\dfrac{3}{2}\cos2\theta + \cos 4\theta \right) + T\dfrac{3 a^4}{2 r^4}\cos 4\theta, \\
\sigma_{yy} (r,\theta)&= - T\dfrac{a^2}{r^2}\left(\dfrac{1}{2}\cos2\theta - \cos 4\theta \right) - T\dfrac{3 a^4}{2 r^4}\cos 4\theta, \\
\sigma_{xy} (r,\theta)&= - T\dfrac{a^2}{r^2}\left(\dfrac{1}{2}\sin2\theta + \sin 4\theta \right) + T\dfrac{3 a^4}{2 r^4}\sin 4\theta,\\
u_{x} (r,\theta)&= \alpha_{0} \left(\dfrac{4 r}{a}(1 - \nu)\cos\theta + \dfrac{2 a}{r}(4 (1 - \nu)\cos\theta + \cos 3\theta) - \dfrac{2 a^3}{r^3}\cos 3\theta\right), \\
u_{y}(r,\theta) &= \alpha_{0} \left(\dfrac{r}{a}(-4 \nu)\sin\theta + \dfrac{2 a}{r}(-2(1 - 2\nu)\sin\theta + \sin 3\theta) - \dfrac{2 a^3}{r^3}\sin 3\theta\right),
\end{split}
\label{plate_solution}
\end{equation}
%
where $\alpha_{0} = (1 + \nu)T a / (4 E)$. In all study cases for this problem, the geometry is parametrized by a basis of second order with the following knot vectors:
%
\begin{equation}
\Sigma = \{0,0,0,1,1,1\}, \qquad \Pi = \{0,0,0,0.5,1,1,1\}.
\label{knot_vectors_plate}
\end{equation}
%
%
The corresponding control points are listed in Table~\ref{cpt_plate}. The parametrization consists of two elements (see Fig.~\ref{plate_param}), and remains unchanged during the solution refinement process (denoted by $\mathcal{N}_{2,2}$).
%
Following the notation of paired bases introduced in Example~\ref{exmp:lap_wedge}, we consider the following choices for the solution approximation basis: $(\mathcal{N}_{2,2}, \mathcal{N}_{2,2})$, $(\mathcal{N}_{2,2}, \mathcal{N}_{3,3})$, $(\mathcal{N}_{2,2}, \mathcal{B}_{2,2})$, and $(\mathcal{N}_{2,2}, \mathcal{B}_{3,3})$. Note that B-Spline basis $\mathcal{B}_{2,2}$ is built on knot vectors (\ref{knot_vectors_plate}).
%
We also consider another geometry parameterization which is built on the knot vectors (\ref{knot_vectors_plate}), with the boundary control points as listed in Table~\ref{cpt_plate}, but the weights of two inner points being changed to $w_{2,2} = w_{2,3} = 0.9$. This parameterization will be denoted by $\tilde{\mathcal{N}}_{2,2}$. Thereafter, we consider two more choices for the solution approximation basis: $(\mathcal{N}_{2,2}, \tilde{\mathcal{N}}_{2,2})$, and $(\mathcal{N}_{2,2}, \tilde{\mathcal{N}}_{3,3})$.
%
\end{exmp}
%


%
\begin{exmp}\label{exmp:elas_sphere}
Consider a thick walled sphere geometry with inner radius $r_{1}$ and outer radius $r_2$ (one-eighth of a hollow sphere for symmetry boundary conditions), see Fig. \ref{sphere_geo}.
%
\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/sphere.pdf}
\caption{One-eighth of a sphere, Example~\ref{exmp:elas_sphere}.}\label{sphere_geo}
\end{center}
\end{figure}
%
The coarsest NURBS parametrization of this geometry can be given by degrees $p_{\xi} = 1$, $p_{\eta} = 2$ and $p_{\zeta} = 2$, built upon the knot vectors:
\begin{equation}
\Sigma = \{0, 0, 1, 1\}, \quad \Pi = \{0, 0, 0, 1, 1, 1\}, \quad Z = \{0, 0, 0, 1, 1, 1\},
\end{equation}
with the control points listed in Table~\ref{cpt_sphere}. We will refer to this parametrization as $Q_{1}$. Using the notations introduced earlier, the basis for this parameterization will be denoted by $\mathcal{N}_{1, 2, 2}$.
%
%
We also consider the following choices of the bases for solution approximation: $\mathcal{N}_{1, 2, 2}$, $\mathcal{N}_{2, 2, 2}$, $\mathcal{B}_{1, 2, 2}$, $\mathcal{B}_{2, 2, 2}$, and $\mathcal{B}_{1, 1, 1}$. Note that, we use uniform refinement of $Q_{1}$ for these bases (similar to the case $A_{1}$ of Section~\ref{sec:geo_param}). And, the basis $\mathcal{B}_{1, 1, 1}$ is built on the knot vectors:
\begin{equation}
\Sigma = \{0,0,1,1\}, \,\,\, \Pi = \{0,0,1,1\}, \,\,\, Z = \{0,0,1,1\}.
\end{equation}
%
The problem in consideration is a well known thick-walled pressurized sphere, i.e. the hollow sphere subjected to the following boundary conditions (in spherical coordinates $r, \theta, \phi$):
%
\begin{equation}
\sigma_{rr} = -p_{1} \quad \text{at} \quad r = r_{1}, \qquad
\sigma_{rr} = -p_{2} \quad \text{at} \quad r = r_{2}.
\end{equation}
%
On the rest of the boundary the symmetry conditions are prescribed. The analytical solution is given by
%
\begin{equation}
u_{r} (r) = \left(\alpha_{1} r + \alpha_{2} r^{-2}\right) / \alpha_{3},
\end{equation}
%
where $\alpha_{1} = 2(p_{1} r_1^3 - p_{2} r_2^3) (1 - 2\nu)$, $\alpha_{2} = (p_1 - p_2) (1 + \nu) r_{1}^{3} r_{2}^{3}$, and $\alpha_{3} = 2 E (r_2^3 - r_1^3)$.
\end{exmp}
%


%
\begin{exmp}\label{exmp:lap_pht}
This example is to demonstrate the effectiveness of the proposed method by using weak coupling of bases for geometry and simulation. The geometry is represented by NURBS, whereas the numerical solution is obtained using PHT-splines (see Appendix~\ref{sec:app_PHT} for necessary details).
%
Consider again an annulus region in two-dimensions, see Fig.~\ref{fig:qannu}, described by a quadratic $C^1$ NURBS surface with $3 \times 3$ control points, and following knot vectors on the parametric domain
%
\[\Sigma = [0, 0, 0, 1, 1, 1], \quad \Pi = [0, 0, 0, 1, 1, 1].\]
%
Consider the Poisson problem with homogeneous Dirichlet boundary conditions, and choose the source function such that the exact solution of the problem has the following form
%
\begin{equation}
u (r,\theta)= (r - 1) (r - 2) \theta (\theta - \frac{\pi}{2}) \exp \bigl ( -100 (r \cos \theta - 1)^2 \bigr),\label{u_pht_poisson}
\end{equation}
%
where
\[
r (x,y) = \sqrt{x^2 + y^2}, \quad \theta = \arctan({y/x}).
\]
%
\end{exmp}
%

%
\subsection{Convergence studies of various examples}\label{sec:conv_examples}
%
\subsubsection*{Example~\ref{exmp:lap_annu}}
%
\begin{figure}[!ht]
\centering
\begin{subfigure}[t]{0.3\textwidth}
\scalebox{.54}{\input{Figures/plot_ex_1_1}}
\caption{Geometry parameterizations $Q_{0}$, $A_{1}$, $A_{2}$, $B_{1}$ and $B_{2}$ combined with solution bases $A_{1}$ and $A_{2}$.}\label{example_1_1}
\end{subfigure}
\quad
\begin{subfigure}[t]{0.3\textwidth}
\scalebox{.54}{\input{Figures/plot_ex_1_2}}
\caption{Geometry parameterizations $C_{1}$ and $C_{2}$ combined with solution bases $C_{1}$, $C_{2}$, $A_{1}$, and $A_{2}$.}\label{example_1_2}
\end{subfigure}
\quad
\begin{subfigure}[t]{0.3\textwidth}
\scalebox{.54}{\input{Figures/plot_ex_1_3}}
\caption{B-splines solution bases: $D_{0}$, $D_{1}$, and $D_{2}$.}\label{example_1_3}
\end{subfigure}
\caption{Convergence study for Example~\ref{exmp:lap_annu}.}\label{fig:lap_annu}
\end{figure}
%
The convergence rates in $L^{2}$-norm for all study cases are shown in Figures~\ref{example_1_1}, \ref{example_1_2}, and \ref{example_1_3}. The results are combined as follows.
%

%
In Fig.~\ref{example_1_1}, we have collected three choices of geometry parametrization, namely the coarsest $Q_{0}$, and its variations $A_{1}$, $A_{2}$, $B_{1}$, and $B_{2}$. Since $A_{1}$, $A_{2}$, $B_{1}$, and $B_{2}$ are equivalent to $Q_{0}$ up to knot-insertion and degree-elevation, the convergence results are identical, and the slope of the graph depends only on the lowest degree of the solution approximation ($1$ in the case of $A_{1}$, and $2$ in the case of $A_{2}$).
%

%
In Fig.~\ref{example_1_2}, we show the results for the non-uniform geometry parameterizations $C_{1}$ and $C_{2}$, and the solution bases $C_{1}$, $C_{2}$, $A_{1}$, and $A_{2}$, in comparison with the coarsest geometry parameterization $Q_{0}$ paired with the solution bases $C_{1}$ and $C_{2}$. Despite the slight difference in the results associated with the choice of the geometry representation, all graphs exhibit the expected convergence rates, which is governed by the lowest degree of the solution approximation.
%

%
Finally, in Fig.~\ref{example_1_3}, we included the results for the fifth case (represented by $T^{5}$ in Table~\ref{tab:res_tests}), characterized by the fact that the B-splines based solution basis can not represent the geometry exactly. The pairs $A_{1}$-$D_{0}$, $A_{1}$-$D_{1}$, $A_{1}$-$D_{2}$ are shown in comparison with $A_{1}$-$A_{1}$ and $A_{1}$-$A_{2}$. Despite the difference in weights of the basis functions between $A_{1}$-$A_{1}$ and $A_{1}$-$D_{1}$ (as well as between $A_{1}$-$D_{2}$ and $A_{1}$-$A_{2}$) the difference in the results is very minor. This makes us conclude that, though the B-splines solution basis fails the patch test, it is nevertheless a suitable basis for the analysis. Moreover, the case $A_{1}$-$D_{0}$, which can be regarded as truly super-parametric in FEA context, also exhibits the expected convergence rate. This surprising result counters the established practice of FEM, where super-parametric approach is not recommended \cite{ZienkiewiczTZ-FEM_V1}.
%


%
\subsubsection*{Example~\ref{exmp:elas_cyl}}
%
\begin{figure}[!ht]
\centering
\begin{subfigure}[t]{0.3\textwidth}
\scalebox{.54}{\input{Figures/plot_ex_3_1}}
\caption{Geometry parameterizations $Q_{0}$, $A_{1}$, $A_{2}$, $B_{1}$ and $B_{2}$ combined with solution bases $A_{1}$ and $A_{2}$.}\label{example_elasticity_1_plot_1}
\end{subfigure}
\quad
\begin{subfigure}[t]{0.3\textwidth}
\scalebox{.54}{\input{Figures/plot_ex_3_2}}
\caption{Geometry parameterizations $C_{1}$ and $C_{2}$ combined with solution bases $C_{1}$, $C_{2}$, $A_{1}$, and $A_{2}$.}\label{example_elasticity_1_plot_2}
\end{subfigure}
\quad
\begin{subfigure}[t]{0.3\textwidth}
\scalebox{.54}{\input{Figures/plot_ex_3_3}}
\caption{B-splines solution bases: $D_{0}$, $D_{1}$, and $D_{2}$.}\label{example_elasticity_1_plot_3}
\end{subfigure}
\caption{Convergence study for Example~\ref{exmp:elas_cyl}.}\label{fig:elas_cyl}
\end{figure}
%
The results of numerical simulations are organized in three plots: Fig.~\ref{example_elasticity_1_plot_1}, \ref{example_elasticity_1_plot_2} and \ref{example_elasticity_1_plot_3}. The obtained numerical solutions exhibit a similar pattern as that observed in Example~\ref{exmp:lap_annu}, i.e., the graphs show that the convergence rate of the solution is defined by the lowest degree in the solution basis, independent of the (exact) geometry parameterization.
%

%
\subsubsection*{Example~\ref{exmp:lap_wedge}}
%
In Fig.~\ref{fig:example_3_Laplace}, we present the results for the following $7$ cases: $(\mathcal{N}_{1,4}, \mathcal{N}_{1,4})$, $(\mathcal{N}_{1,4}, \mathcal{B}_{1,4})$, $(\mathcal{N}_{1,4}, \mathcal{N}_{2,4})$, $(\mathcal{N}_{1,4}, \mathcal{B}_{2,4})$, $(\mathcal{N}_{1,4}, \mathcal{B}_{1,1})$, $(\mathcal{N}_{1,4}, \mathcal{B}_{1,2})$, and $(\mathcal{N}_{1,4}, \mathcal{B}_{1,3})$. 
%
As we can observe from the numerical studies presented in Fig.~\ref{fig:example_3_Laplace}, together with the exact representation of the geometry, the convergence rate depends only on the lowest degree in the solution approximation, and the results for NURBS and B-splines solution bases are almost identical.
%
\begin{figure}[!ht]
\begin{center}
\include{Figures/plot_ex_2_1}
\caption{Convergence study for Example~\ref{exmp:lap_wedge}.}
\label{fig:example_3_Laplace}
\end{center}
\end{figure}
%


%
\subsubsection*{Example~\ref{exmp:elas_plate}}
%
The results of these study cases are shown in Fig.~\ref{example_elasticity_2_plot_1}, where it can be seen that the convergence rate for all choices of the solution basis depends only on the order of the solution basis, and for the bases of the same order, the results for different basis functions are almost identical.
%
\begin{figure}[!ht]
\begin{center}
\input{Figures/plot_ex_4_1}
\caption{Convergence study for Example~\ref{exmp:elas_plate}.}
\label{example_elasticity_2_plot_1}
\end{center}
\end{figure}
%


%
\subsubsection*{Example~\ref{exmp:elas_sphere}}
The numerical results are shown in Fig.~\ref{convergence3D}, where it can again be seen that the convergence rate in all the five cases depends only on the approximation basis for the numerical solution.
%
\begin{figure}[!ht]
\begin{center}
\input{Figures/plot_ex_5_3D}
\caption{Convergence study for Example \ref{exmp:elas_sphere}.}\label{convergence3D}
\end{center}
\end{figure}
%


%
\subsubsection*{Example~\ref{exmp:lap_pht}}
%
In Fig.~\ref{fig:Annus2}, we show the results during local refinement operations. From the first row to the fourth row, we show the T-mesh on the parametric domain (left), the mesh on the physical domain (middle), and the corresponding exact error color-map on the physical domain (right). In Fig.~\ref{fig:AnnusPHTerror}, we present the convergence behavior of four choices of geometry and simulation bases, which are as follows:
%
\begin{itemize}
\item (1) IGA with cubic NURBS (for the geometry as well as the numerical solution). Note that a NURBS of $p_{\xi} = 1$ and $p_{\eta} = 2$ is sufficient for this geometry. However, to have a fair comparison with the remaining studies, we elevate the degree to $p_{\xi} = p_{\eta} = 3$ while maintaining the exact geometry representation. In this example, during the $h$-refinement we inserted each knot twice to have the $C^{1}$-continuous basis for a fair comparison with the PHT splines.  
%
\item (2) GIFT with NURBS of $p_{\xi} = 1$ and $p_{\eta} = 2$ for the geometry, and cubic B-Splines for the numerical solution. In this example, during the $h$-refinement of the solution basis, we inserted each knot twice to have the $C^{1}$-continuous basis. The NURBS geometry was kept unchanged during the solution refinement process.
%
\item (3, 5) IGA with cubic PHT-splines (for the geometry as well as the numerical solution). Note that, in this case, the computational geometry is only approximate (not exact as in IGA with cubic NURBS). In this example, both, uniform (3) and adaptive (5) refinements were performed. 
%
\item (4, 6) GIFT with NURBS of $p_{\xi} = 1$ and $p_{\eta} = 2$ for the geometry and cubic PHT-splines for the numerical solution. In this example, both, uniform (4) and adaptive (6) refinements of the PHT-splines basis were also performed. 

\end{itemize}
%
From the convergence plots, we conclude the following:
\begin{enumerate}
\item Analogously to the results observed in earlier studies, IGA with cubic NURBS (1), GIFT with cubic B-splines (2) and both, IGA (3) and GIFT (4) with cubic uniformly refined PHT-splines exhibit same convergence rate. This is because all three solution bases are of the same degree. Note that convergence curves (1), (2), (3), (4) are quasi-identical, meaning that the error is influenced by the solution basis significantly more than by the choice of the domain parameterization.

\item Owing to the local adaptive refinement of the solution basis, both the cases of PHT-splines (IGA as well as the proposed GIFT, i.e., (5 and 6)) exhibit slight improvement in the convergence rate of the overall error than the same examples with uniform refinement of the PHT-splines basis (3 and 4).

\item The comparison of IGA with PHT-splines (3, 5) and GIFT with PHT-splines (4, 6) solution highlights an important difference. The advantage of the exact geometry representation in the latter case over an approximate geometry in the former case is very minor. This is due to the fact that the geometry of the computational domain is simple, and can be accurately approximated with the PHT-splines of third degree (geometry approximation error is below the discretization error). However, in realistic industrial problems with complex domains, this advantage will become more pronounced. In comparison to standard IGA with PHT splines, employing the exact coarse NURBS geometry parametrization in GIFT (together with PHT-splines solution) brings two distinct advantages:
%
\begin{itemize}
\item It eliminates the need to communicate with the original CAD model at each step of the solution refinement process, and the approximation of the boundaries.
\item It also eliminates the need to refine the original coarse geometry, as well as to store and process the refined data, which can lead to significant computational savings for big problems.
\end{itemize} 
\end{enumerate}
%
%
\setlength{\picw}{1.8in}
\setlength{\picwt}{2.1in}
\begin{figure}
\centering
\includegraphics[width=\picw]{Figures/mesh_01156_1}
\quad
\includegraphics[width=\picw]{Figures/mesh_01156_2}
\quad
\includegraphics[width=\picwt]{Figures/mesh_01156_3}
\\
\includegraphics[width=\picw]{Figures/mesh_03008_1}
\quad
\includegraphics[width=\picw]{Figures/mesh_03008_2}
\quad
\includegraphics[width=\picwt]{Figures/mesh_03008_3}
\\
\includegraphics[width=\picw]{Figures/mesh_07688_1}
\quad
\includegraphics[width=\picw]{Figures/mesh_07688_2}
\quad
\includegraphics[width=\picwt]{Figures/mesh_07688_3}
\\
\includegraphics[width=\picw]{Figures/mesh_11428_1}
\quad
\includegraphics[width=\picw]{Figures/mesh_11428_2}
\quad
\includegraphics[width=\picwt]{Figures/mesh_11428_3}
\caption{Example~\ref{exmp:lap_pht}. GIFT approach for a problem with exact solution having sharp peaks. The geometry representation is based on the coarsest NURBS with $p_{\xi} = 1$ and $p_{\eta} = 2$, and the numerical solution is based on $C^{1}$ PHT-splines, the latter enabling local refinement. From the first row to the fourth row, we show the T-mesh in the parametric domain (left), the mesh on the physical domain (middle), and the corresponding error color-map on physical domain during local refinement operations (right).}
\label{fig:Annus2}
\end{figure}
%
\begin{figure}[t]
\centering
\input{Figures/plot_conv}
\caption{Convergence study for Example~\ref{exmp:lap_pht}. IGA method with cubic NURBS, IGA method with cubic PHT-splines, GIFT method with cubic B-splines, and GIFT method with cubic PHT-splines (as in Fig.~\ref{fig:Annus2}).}
\label{fig:AnnusPHTerror} 
\end{figure}
%

%
\subsection{Discussion on numerical results}\label{sec:res_disc}
%
The results of Section~\ref{patch_tests} indicate that a \textbf{sufficient condition} for the combination of the geometry and the solution bases to pass the \textbf{classical patch test} is the requirement that these two bases are equivalent up to the degree elevation \eqref{degree_elevation_geo_exactness} and/or knot insertion operations \eqref{knot_insertion_geo_exactness}, which ensure the preservation of the geometry exactness. % 
%
However, we see from the numerical results in Sections~\ref{sec:conv_examples}, that despite failing the classical patch test, all  cases presented in Table~\ref{tab:res_tests} exhibit optimal order convergence. This includes the bases derived from the geometry parametrization $A$ and $B$, the non-uniform parametrization $C$, and the B-splines basis $D$ (which cannot represent the geometry exactly). This is not surprising, however. Thanks to \cite[Lemma~3.4-3.5, Theorem~3.1-3.2]{HughesErrorIGA}, with a suitable projector $\Pi_{\mathcal{V}_{h}}$, the optimal order global error estimates hold for every function $v \in H^{l} (\Omega)$. The effect of geometry mapping is included in the definition of the element size $h_{K} = \Vert \nabla F \Vert_{L^{\infty}(Q)} h_{Q}$, where $F$ is the geometry mapping, $Q$ is the element in the parameteric domain, and $h_{Q}$ is the element size in the parameteric domain. Moreover, the constant $C_{\text{shape}}$ appearing in \cite[Lemma~3.4-3.5, Theorem~3.1-3.2]{HughesErrorIGA} depends only on the weight function $w$ and its reciprocal $1/w$ on $\tilde{Q}$, where $\tilde{Q}$ is the support extension of $Q$, and is uniformly bounded with respect to the mesh size. This observation is in alignment with \cite{Stummel-80} that the classical patch test is neither necessary nor sufficient for convergence. Therefore, together with exact geometric representation, a suitable basis (derived-from or related-to geometry parameterization) can be used for optimal order convergence.
%
For a quick comparison, in Table~\ref{tab:pt_oc} we recall all the cases studied in this paper.
%
\begin{table}[!ht]
\renewcommand*{\arraystretch}{1.3}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline 
Geometry & Solution & Degree & Patch & Optimal \\
parameterization & basis & parity & test & convergence \\
\hline 
$Q_{0}$ & $A_{1}$ & Iso-geometric & \checkmark & \checkmark \\
$Q_{0}$ & $A_{2}$ & Super-geometric & \checkmark & \checkmark \\
$Q_{0}$ & $C_{1}$ & Iso-geometric & $\times$ & \checkmark \\
$Q_{0}$ & $C_{2}$ & Super-geometric & $\times$ & \checkmark \\
\hline \hline
$A_{1}$ & $A_{1}$ & Iso-geometric & \checkmark & \checkmark \\
$A_{1}$ & $A_{2}$ & Super-geometric & \checkmark & \checkmark \\
$A_{2}$ & $A_{1}$ & Sub-geometric & \checkmark & \checkmark \\
\hline \hline
$B_{1}$ & $A_{1}$ & Iso-geometric & \checkmark & \checkmark \\
$B_{1}$ & $A_{2}$ & Super-geometric & \checkmark & \checkmark \\
$B_{2}$ & $A_{1}$ & Sub-geometric & \checkmark & \checkmark \\
\hline \hline
$C_{1}$ & $C_{1}$ & Iso-geometric & \checkmark & \checkmark \\
$C_{1}$ & $C_{2}$ & Super-geometric & \checkmark & \checkmark \\
$C_{2}$ & $C_{1}$ & Sub-geometric & \checkmark & \checkmark \\
\hline \hline
$C_{1}$ & $A_{1}$ & Iso-geometric & $\times$ & \checkmark \\
$C_{1}$ & $A_{2}$ & Super-geometric & $\times$ & \checkmark \\
$C_{2}$ & $A_{1}$ & Sub-geometric & $\times$ & \checkmark \\
\hline \hline
$A_{1}$ & $D_{1}$ & Iso-geometric & $\times$ & \checkmark \\
$A_{1}$ & $D_{2}$ & Super-geometric & $\times$ & \checkmark \\
$A_{1}$ & $D_{0}$ & Sub-geometric & $\times$ & \checkmark \\
\hline
\end{tabular} 
\caption{Summary of patch tests and optimal convergence. See Section~\ref{sec:geo_param} for the notations $Q$, $A$, $B$, $C$, and $D$. For the degree parity, we used the naming convention with respect to the geometry, see Table~\ref{tab:naming}.}
\label{tab:pt_oc}
\end{center}
\end{table}
%


%
Nevertheless, it is important to exercise caution while devising a basis for the numerical solution. In all our numerical examples \ref{exmp:lap_annu}-\ref{exmp:elas_sphere}, the solution basis was constructed based on the same knot vectors as the geometry parametrization. This assures the continuity of the geometry parametrization within solution elements. For Example~\ref{exmp:elas_plate}, we now consider the coarsest basis consisting of B-splines of degree $2 \times 2$ defined on the knot vectors (denoted by $\tilde{\mathcal{B}}_{2,2}$):
\begin{equation}
\Sigma = \{0, 0, 0, 1, 1, 1\}, \quad \Pi = \{0, 0, 0, 0.166667, 1, 1, 1\}.
\label{knot_vectors_example_divergence_plate}
\end{equation}
%
In this case, the singular point $(x = L, ~y = L)$ is inside an element of any mesh for the numerical solution. As it can be seen from Fig.~\ref{example_elasticity_2_plot_2}, the solution does not exhibit the expected convergence rate.
%
\begin{figure}[!ht]
\begin{center}
\input{Figures/plot_ex_4_2}
\caption{Convergence study for Example~\ref{exmp:elas_plate} with knot vectors \eqref{knot_vectors_example_divergence_plate}.}
\label{example_elasticity_2_plot_2}
\end{center}
\end{figure}
%
Though a thorough mathematical derivation of the presented approach is a subject of future research, but we recommended to avoid those combination of geometry-solution bases where the continuity of the geometry parametrization is violated in the solution elements. 
%

%
\subsection{Naming convention}\label{sec:naming}

Let $p_{u}$ and $p_{g}$ denote the degrees of the basis functions for the solution, and the geometry, respectively. In the standard FE context, since the primary quantity of interest is the numerical solution, the naming convention is with respect to the parameter $p_{u}$. However, in IGA, the primary quantity is the geometry. Accordingly, for better readability, we propose the naming convention with respect to the parameter $p_{g}$, as presented in Table~\ref{tab:naming}. % 
%
\begin{table}[!ht]
\begin{center}
\begin{tabular}{|c|c|c|c|}\hline
& $p_{u} = p_{g}$ & $p_{u} < p_{g}$ & $p_{u} > p_{g}$ \\ \hline
FEM & Iso-parametric & Super-parametric & Sub-parametric \\
GIFT & Iso-geometric & Sub-geometric & Super-geometric \\
\hline
\end{tabular}
\end{center}
\caption{Naming convention}
\label{tab:naming}
\end{table}
%

%
Note that, in standard FEM, the use of $p_{u} < p_{g}$ (super-parameteric) case is not recommended \cite[P.172]{ZienkiewiczTZ-FEM_V1}. However, the results presented in Example~\ref{exmp:lap_wedge} show that, with exact geometry representation, super-parametric approximations can also deliver optimal orders of convergence.
%
