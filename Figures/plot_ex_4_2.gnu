reset
set terminal epslatex size 3.5, 4.0 color colortext 10
set output 'plot_ex_4_2.tex'
      
set logscale x                              # remove any log-scaling
set logscale y
unset label                            # remove any previous labels
set xtics font ", 18" offset 0, graph -0.0
set ytics font ", 18" offset -0.0, 0.0
# set format y '%.0lx10^{%S}';
set format y '%4.2e'
set title " "
set xlabel "$\\log(h)$"
set ylabel "$\\log(|| e ||_{L^{2}(\\Omega)})$"
unset key

set key at 0.1, 5.0
set key font ", 18"
set key spacing 1.6
set lmargin at screen 0.2
set style line 1 lc rgb '#0072bd' lt 1 lw 2 pt 1 ps 1.5
set style line 2 lc rgb '#d95319' lt 1 lw 2 pt 2 ps 1.5
set style line 3 lc rgb '#edb120' lt 1 lw 2 pt 3 ps 1.5
set style line 4 lc rgb '#7e2f8e' lt 1 lw 2 pt 4 ps 1.5
set style line 5 lc rgb '#77ac30' lt 1 lw 2 pt 5 ps 1.5
set style line 6 lc rgb '#4dbeee' lt 1 lw 2 pt 6 ps 1.5
set style line 7 lc rgb '#a2142f' lt 1 lw 2 pt 7 ps 1.5
set style line 8 lc rgb '#808000' lt 1 lw 2 pt 8 ps 1.5
set style line 9 lc rgb '#006400' lt 1 lw 2 pt 9 ps 1.5
set style line 10 lc rgb '#8B008B' lt 1 lw 2 pt 10 ps 1.5

set yrange[1.0e-3 : 8.0]

plot \
"/Users/tomar/CS112p/ProjRes/WIGA/Suppl_WIGA_NURBS/Example_2_Elasticity/Results_all/ISO_BSplines_2.csv" with linespoints linestyle 1 title '($\mathcal{N}_{2,2}, \tilde{\mathcal{B}}_{2,2}$)'
