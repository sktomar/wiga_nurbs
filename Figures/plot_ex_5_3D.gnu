reset
set terminal epslatex size 3.5, 4.0 color colortext 10
set output 'plot_ex_5_3D.tex'

set logscale x                              # remove any log-scaling
set logscale y
unset label                            # remove any previous labels
set xtics font ", 18" offset 0, graph -0.0
set ytics font ", 18" offset -0.0, 0.0
set format y '%4.2e' # set format y '%.0lx10^{%S}';
set title " "
set xlabel "$\\log(h)$"
set ylabel "$\\log(|| e ||_{L^{2}(\\Omega)})$"
unset key

set key at 1.25, 0.9
set key font ", 18"
set key spacing 1.6
set lmargin at screen 0.2
set style line 1 lc rgb '#0072bd' lt 1 lw 2 pt 1 ps 1.5
set style line 2 lc rgb '#d95319' lt 1 lw 2 pt 2 ps 1.5
set style line 3 lc rgb '#edb120' lt 1 lw 2 pt 3 ps 1.5
set style line 4 lc rgb '#7e2f8e' lt 1 lw 2 pt 4 ps 1.5
set style line 5 lc rgb '#77ac30' lt 1 lw 2 pt 5 ps 1.5
set style line 6 lc rgb '#4dbeee' lt 1 lw 2 pt 6 ps 1.5
set style line 7 lc rgb '#a2142f' lt 1 lw 2 pt 7 ps 1.5
set style line 8 lc rgb '#808000' lt 1 lw 2 pt 8 ps 1.5
set style line 9 lc rgb '#006400' lt 1 lw 2 pt 9 ps 1.5
set style line 10 lc rgb '#8B008B' lt 1 lw 2 pt 10 ps 1.5
      
set xrange[0.225 : 1.25]
set yrange[0.5e-4 : 1.0]
a2 = 2;
X1 = 0.26;
X2 = X1 + 0.15;
Y1 = 0.0175;
Y2 = Y1 * 10**(a2 * log10(X1 / X2) );

set object 1 poly from X1, Y1 to X2, Y1 to X1, Y2 to X1, Y1 lw 2 fs empty border lc rgb '#0060ad'
set label "1" at 1.2*X1, 1.3*Y1 font ", 18" tc ls 1
set label "2" at 0.925*X1, 0.65*Y1 font ", 18" tc ls 1

a3 = 3;
X3 = 0.4;
X4 = X3 - 0.15;
Y3 = 1e-4;
Y4 = Y3 * 10**(a3 * log10(X3 / X4) );

set object 2 poly from X3, Y3 to X4, Y3 to X3, Y4 to X3, Y3 lw 2 fs empty border lc rgb 'red'
set label "1" at 0.8*X3, 0.7*Y3 font ", 18" tc ls 2
set label "3" at 1.05*X3, 1.75*Y3 font ", 18" tc ls 2

plot \
"/Users/tomar/CS112p/ProjRes/WIGA/Suppl_WIGA_NURBS/Example_3D/Q0_ISO_BSplines.csv" with linespoints linestyle 1 title '($\mathcal{N}_{1,2,2}, \mathcal{B}_{1,2,2}$)', \
"/Users/tomar/CS112p/ProjRes/WIGA/Suppl_WIGA_NURBS/Example_3D/Q0_ISO_NURBS.csv" with linespoints linestyle 2 title '($\mathcal{N}_{1,2,2}, \mathcal{N}_{1,2,2}$)', \
"/Users/tomar/CS112p/ProjRes/WIGA/Suppl_WIGA_NURBS/Example_3D/Q0_SUB_BSplines.csv" with linespoints linestyle 3 title '($\mathcal{N}_{1,2,2}, \mathcal{B}_{1,1,1}$)', \
"/Users/tomar/CS112p/ProjRes/WIGA/Suppl_WIGA_NURBS/Example_3D/Q0_SUPER_BSplines.csv" with linespoints linestyle 4 title '($\mathcal{N}_{1,2,2}, \mathcal{B}_{2,2,2}$)', \
"/Users/tomar/CS112p/ProjRes/WIGA/Suppl_WIGA_NURBS/Example_3D/Q0_SUPER_NURBS.csv" with linespoints linestyle 5 title '($\mathcal{N}_{1,2,2}, \mathcal{N}_{2,2,2}$)', \
