reset
set terminal postscript eps size 3.5, 2.62 enhanced color font 'Helvetica,20' linewidth 2
set output '/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Figures/plot_A1.eps'

set size square                  # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtics font ", 18" offset 0, graph -0.0
set ytics font ", 18" offset -0.0, 0.0
set title " "
set xlabel " "
set ylabel " "
unset key
set parametric
set dummy t
set trange [0 : 1]
set style line 1 linecolor rgb '#000000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#4169E1' linetype 1 linewidth 2 dashtype 3
set style line 3 lc rgb 'red' lt 1 lw 1 pt 7 pi -1 ps 2.0
plot cos(t*pi*0.5), sin(t*pi*0.5) with lines linestyle 1, \
2*cos(t*pi*0.5), 2*sin(t*pi*0.5) with lines linestyle 1, \
1.6666*cos(t*pi*0.5), 1.6666*sin(t*pi*0.5) with lines linestyle 2, \
0.9833 + t*(1.9676 - 0.9833), 0.1818 + t*(0.3639 - 0.1818) with lines linestyle 2,\
0.0, 1 + t with lines linestyle 1, 1 + t, 0.0 with lines linestyle 1, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_regular_3.csv" with lines linestyle 3, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_regular_4.csv" with lines linestyle 3, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_regular_0.csv" with linespoints linestyle 3, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_regular_1.csv" with linespoints linestyle 3, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_regular_2.csv" with linespoints linestyle 3
