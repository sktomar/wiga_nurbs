reset
set terminal epslatex size 4.5, 4.0 color colortext 10
#       set terminal latex      
set output 'plot_qannu.tex'
set size square                  # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtics font ", 24" offset 0,graph -0.05
set ytics font ", 24" offset -1,0
set title " "
set xlabel " "
set ylabel " "
unset key
set parametric
set dummy t
set trange [0:1]
set style line 1 linecolor rgb '#000000' linetype 1 lw 10
set style line 2 linecolor rgb '#000000' linetype 1 lw 3

set arrow to 1.1,1.1 ls 2

set label 1 "$r$" at 0.4,0.6
set label 2 "$\\theta$" at 0.25,0.1
set label 3 "$(x, y)$" at 1.1,1.2

plot cos(t*pi*0.5),sin(t*pi*0.5) with lines linestyle 1, \
    2*cos(t*pi*0.5),2*sin(t*pi*0.5) with lines linestyle 1, \
    0.2*cos(t*pi*0.5/2),0.2*sin(t*pi*0.5/2) with lines linestyle 2, \
    0.0,1 + t with lines linestyle 1, \
    1 + t,0.0 with lines linestyle 1 \
