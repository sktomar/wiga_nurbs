      set term qt 1 size 700,700      
      #set size square                  # scale axes automatically
      set logscale x                              # remove any log-scaling
      set logscale y
      unset label                            # remove any previous labels
      set xtics font ", 16" offset -1, graph -0.01
      set ytics font ", 16" offset -1,0 
      set format x "%.0lx10^{%S}";
      set format y "%.0lx10^{%S}";
      set title " "
      set xlabel font " DOF, 16"
      set ylabel " "
      unset key
      set key at 1000000, 0.1
      set key font "ariel, 16" 
      set lmargin at screen 0.2
      set style line 1 lc rgb '#0072bd' lt 1 lw 2 pt 1 ps 1.5
      set style line 2 lc rgb '#d95319' lt 1 lw 2 pt 2 ps 1.5
      set style line 3 lc rgb '#edb120' lt 1 lw 2 pt 3 ps 1.5
      set style line 4 lc rgb '#7e2f8e' lt 1 lw 2 pt 4 ps 1.5
      set style line 5 lc rgb '#77ac30' lt 1 lw 2 pt 5 ps 1.5
      set style line 6 lc rgb '#4dbeee' lt 1 lw 2 pt 6 ps 1.5
      set style line 7 lc rgb '#a2142f' lt 1 lw 2 pt 7 ps 1.5
      set style line 8 lc rgb '#808000' lt 1 lw 2 pt 8 ps 1.5
      set style line 9 lc rgb '#006400' lt 1 lw 2 pt 9 ps 1.5
      set style line 10 lc rgb '#8B008B' lt 1 lw 2 pt 10 ps 1.5

      a2 = 2;      
      X1 = 10000;
      Y1 = 0.0001;
      X2 = 30000;
      Y2 = 0.0000111111;


      #set object 1 poly from X1,Y1 to X2,Y1 to X1,Y2 to X1,Y1 fs empty border 1 
      set object 1 poly from X1,Y1 to X2,Y1 to X2,Y2 to X1,Y1 lw 2 fs empty border lc rgb '#0060ad'
      set label "1" at X1 + 0.25*(X2 - X1), 1.5*Y1 font "ariel, 16" tc ls 1
      set label "2" at 1.1*X2, 0.35*Y1 font "ariel, 16" tc ls 1
       
      
plot "/home/eliana/Documents/My papers/GIFT/Example_PHT/NEW/error_plot_IGA_NURBS.log" with linespoints linestyle 10 title "(1) IGA: NURBS, uniform", \
"/home/eliana/Documents/My papers/GIFT/Example_PHT/NEW/error_plot_GIFT_NURBS_BSPLINES.log" with linespoints linestyle 3 title "(2) GIFT: NURBS + BSplines, uniform", \
"/home/eliana/Documents/My papers/GIFT/Example_PHT/NEW/error_plot_IGA_PHT_uniform.log" with linespoints linestyle 4 title "(3) IGA: PHT, uniform", \
"/home/eliana/Documents/My papers/GIFT/Example_PHT/NEW/error_plot_IGA_PHT_20_biggest.log" with linespoints linestyle 5 title "(4) IGA: PHT, adaptive", \
"/home/eliana/Documents/My papers/GIFT/Example_PHT/NEW/error_plot_GIFT_NURBS_PHT_uniform.log" with linespoints linestyle 6 title "(5) GIFT: NURBS + PHT, uniform", \
"/home/eliana/Documents/My papers/GIFT/Example_PHT/NEW/error_plot_GIFT_NURBS_PHT_20_biggest.log" with linespoints linestyle 7 title "(6) GIFT: NURBS + PHT, adaptive", \

