reset
set terminal epslatex size 4.5, 4.0 color colortext 10
set output 'plot_conv.tex'
      
set logscale x                              # remove any log-scaling
set logscale y
unset label                                 # remove any previous labels
# set xtics font ", 18" offset 7.5, graph -0.2
# set ytics font ", 18" offset -0.0, 0.0
# set format y '%.0lx10^{%S}';
set format x '%4.0e'
set format y '%4.2e'
# set title " "
# set xlabel "$\\log(h)$"
set xlabel "DOF"
set ylabel "$\\log(|| e ||_{L^{2}(\\Omega)})$"
unset key
set key at 75000, 0.05
set key font ", 18"
set key spacing 1.5

set lmargin at screen -0.0
set style line 1 lc rgb '#0072bd' lt 1 lw 2 pt 1 ps 1.5
set style line 2 lc rgb '#d95319' lt 1 lw 2 pt 2 ps 1.5
set style line 3 lc rgb '#edb120' lt 1 lw 2 pt 3 ps 1.5
set style line 4 lc rgb '#7e2f8e' lt 1 lw 2 pt 4 ps 1.5
set style line 5 lc rgb '#77ac30' lt 1 lw 2 pt 5 ps 1.5
set style line 6 lc rgb '#4dbeee' lt 1 lw 2 pt 6 ps 1.5
set style line 7 lc rgb '#a2142f' lt 1 lw 2 pt 7 ps 1.5
set style line 8 lc rgb '#808000' lt 1 lw 2 pt 8 ps 1.5
set style line 9 lc rgb '#006400' lt 1 lw 2 pt 9 ps 1.5
set style line 10 lc rgb '#8B008B' lt 1 lw 2 pt 10 ps 1.5

set yrange[0.5e-6 : 0.1e-0]
a2 = 2;
X1 = 6000.0;
X2 = 3*X1;
Y1 = 0.0001;
Y2 = Y1 * 10**(a2 * log10(X1 / X2) );

set object 1 poly from X1, Y1 to X2, Y1 to X2, Y2 to X1, Y1 lw 2 fs empty border lc rgb '#0060ad'
set label "1" at X1 + 0.4*(X2 - X1), 1.5*Y1 font ", 18" tc ls 1
set label "2" at 1.35*X2, 0.35*Y1 font ", 18" tc ls 1

#/Users/tomar/CS112p/ProjRes/WIGA/20171105/Example_PHT
      
plot \
"/Users/tomar/CS112p/Publications/WIGA_NURBS/Review/201711/Figures/data_convergence/error_plot_IGA_NURBS.txt" with linespoints linestyle 10 title '(1) IGA: NURBS, uniform', \
"/Users/tomar/CS112p/Publications/WIGA_NURBS/Review/201711/Figures/data_convergence/error_plot_GIFT_NURBS_BSplines.txt" with linespoints linestyle 3 title '(2) GIFT: NURBS + BSPlines, uniform', \
"/Users/tomar/CS112p/Publications/WIGA_NURBS/Review/201711/Figures/data_convergence/error_plot_IGA_PHT_uniform.txt" with linespoints linestyle 4 title '(3) IGA: PHT, uniform', \
"/Users/tomar/CS112p/Publications/WIGA_NURBS/Review/201711/Figures/data_convergence/error_plot_GIFT_NURBS_PHT_uniform.txt" with linespoints linestyle 6 title '(4) GIFT: NURBS + PHT, uniform', \
"/Users/tomar/CS112p/Publications/WIGA_NURBS/Review/201711/Figures/data_convergence/error_plot_IGA_PHT_20_biggest.txt" with linespoints linestyle 5 title '(5) IGA: PHT, adaptive', \
"/Users/tomar/CS112p/Publications/WIGA_NURBS/Review/201711/Figures/data_convergence/error_plot_GIFT_NURBS_PHT_20_biggest.txt" with linespoints linestyle 7 title '(6) GIFT: NURBS + PHT, adaptive'
