reset
set terminal postscript eps size 3.5, 2.62 enhanced color font 'Helvetica,20' linewidth 2
set output '/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Figures/plot_Q0.eps'

set size square                  # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtics font ", 18" offset 0, graph -0.0
set ytics font ", 18" offset -0.0, 0.0
set title " "
set xlabel " "
set ylabel " "
unset key
set xrange [0 : 2.1]
set yrange [0 : 2.1]
set parametric
set dummy t
set trange [0 : 1]
set style line 1 linecolor rgb '#000000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#4169E1' linetype 1 linewidth 2 dashtype 3
set style line 3 lc rgb 'red' lt 1 lw 1 pt 7 pi -1 ps 2.0
plot cos(t*pi*0.5),sin(t*pi*0.5) with lines linestyle 1, \
2*cos(t*pi*0.5), 2*sin(t*pi*0.5) with lines linestyle 1, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_Q0_1.csv" with linespoints linestyle 3, \
"/Users/tomar/CS112p/ProjRes/WIGA/WIGA_NURBS/Suppl/cpt_Q0_2.csv" with linespoints linestyle 3, \
      0.0, 1 + t with lines linestyle 1, 1 + t, 0.0 with lines linestyle 1
